import React from 'react';
import '../css/listaTareas.css'
import Indice from './Indice';

const ListaCursos = () => {
  return (

    <div className='container-fluid' style={{ padding: 0 }}>
      <Indice />

      <div className='col-12' id='usuario' style={{ width: '50%', marginTop: '100px' }}><br />
        <center><h1>LISTA DE CURSOS</h1></center>
        <br />
        <div className='tabla'>
          <table className="table table-bordered">
            <thead className="thead-dark">
              <tr>
                <th>NOMBRE DEL CURSO</th>
                <th>PERIODO</th>
                <th>VER</th>
              </tr>
            </thead>
            <tbody style={{ backgroundColor: '#ffffff' }}>
              <tr>
                <td>5to Ciclo</td>
                <td>ABR23-SEP23</td>
                <td>
                  <a className='btn btn-primary' href={'/TareasCreadas'} role="button" >Ver Curso</a>
                </td>
              </tr>
              <tr>
                <td>4to Ciclo</td>
                <td>ABR23-SEP23</td>
                <td>
                  <a className='btn btn-primary' href={'/TareasCreadas'} role="button" >Ver Curso</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </div>

  );
};

export default ListaCursos;