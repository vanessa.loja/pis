
import { useNavigate } from 'react-router';
import '../css/visualizar.css'
import Indice from './Indice';
function CursosCompletados() {
    const navigate = useNavigate();

    return (
        <div className='container-fluid' style={{ padding: 0 }}>
            <Indice />
            <div class="list-group">
                <h1>Lista de Tareas</h1>
                <a href="#" class="list-group-item list-group-item-action" id="il" aria-current="true">
                    El elemento de enlace actual
                </a>
                <a href="" class="list-group-item list-group-item-action" id="il">Un segundo elemento de enlace</a>
                <a href="#" class="list-group-item list-group-item-action" id="il">Un tercer elemento de enlace</a>
                <a href="#" class="list-group-item list-group-item-action" id="il">Un cuarto elemento de enlace</a>
                <a href="#" class="list-group-item list-group-item-action" id="il">Un elemento de enlace desactivado</a>
            </div>

        </div>

    );
}
export default CursosCompletados;