import React from 'react';
import '../css/Indice.css';
import { useNavigate } from 'react-router';
import CursosCompletados from './Cursoscompletados';
import ListaTareasPendientes from './ListaTareasPendientes';
const Indice = () => {
  const navigation = useNavigate();

  const handleAtrasados = () => {
    navigation('/ListarTareasAtrasadas');
  };

  const handleCursos = () => {
    navigation('/Cursoscompletados');
  };

  const handlesalir = () => {
    navigation('/');
  };

  const TareasCreadas = () => {
    navigation('/TareasCreadas');
  };
  return (

    <div className='InicioWindow '>
      <div className='NavSuperior' id='NavSuperior'>
        <nav className="navbar navbar-dark bg-dark navbar-expand-lg navbar-light bg-light">
          <div className="navbar-brand flex-grow-1" style={{ fontFamily: 'Courier New', fontSize: '20px' }}>P r o y e c t o _ I n t e g r a d o r</div>
          <ul className="navbar-nav ml-auto">
            <li className="nav-item dropdown no-arrow">
              <a className="nav-link" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={handlesalir}>Salir</a>
            </li>
            <li className="nav-item dropdown no-arrow ">
              <a className="nav-link mr-9" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Nombre del Usuario logeado</a>
            </li>
          </ul>
        </nav>
      </div>


      <div className='NavLateral' id='NavLateral'>
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

          <li className="nav-item active">
            <a className="sidebar-brand d-flex align-items-center justify-content-center nav-link collapsed" href="ListaCursos" >Cursos</a>
          </li>

          <hr className="sidebar-divider" />

          <li className="nav-item active" style={{ textAlign: 'center' }}>
            <a className="nav-link collapsed align-items-center justify-content-center" style={{ display: 'flex', justifyContent: 'center' }}>Tareas</a>
            <hr className="sidebar-divider" />
            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div className="bg-white py-2 collapse-inner rounded">
                <a className="collapse-item" href="#" style={{ textAlign: 'center' }}>Entregadas</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="#" onClick={handleAtrasados} style={{ textAlign: 'center' }}>Atrasados</a>
              </div>
            </div>
          </li>



          <li className="nav-item">
            <a className="nav-link collapsed" style={{ textAlign: 'center' }} href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              <i className="fas fa-fw fa-cog"></i>
              <span>Cursos</span>
            </a>
            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
              <div className="bg-white py-2 collapse-inner rounded">
                <hr className="sidebar-divider" />
                <a className="collapse-item" style={{ textAlign: 'center' }}>En curso</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="#" >Finalizados</a>
                <hr className="sidebar-divider" />
                <a className="collapse-item" href="cards.html">Acreditaciones</a>

              </div>
            </div>
          </li>
          <hr className="sidebar-divider" />
          <div className="sidebar-heading" style={{ textAlign: 'center' }}>
            Progreso
          </div>

          <li className="nav-item">
            <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages" />
          </li>
        </ul>
      </div>
    </div>


  )
};

export default Indice;
