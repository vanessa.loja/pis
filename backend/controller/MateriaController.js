'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var materia = models.materia;
var rol = models.rol;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const salRounds = 8;
class MateriaController {
   
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var rol_id = req.body.external_rol;
         
            if (1===1) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
                console.log(req.body.email);
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(salRounds), null);
                    };

                    //data arreglo asociativo= es un direccionario = clave:valor
                    var data = {
                        identificacion: req.body.identificacion,
                        tipo_identificacion: req.body.tipo_identificacion,
                        nombres: req.body.nombres,
                        edad: req.body.edad,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        telefono:req.body.telefono,
                        id_rol: rolAux.id,
                        cuenta: {
                            email: req.body.email,
                            clave: claveHash(req.body.clave)
                        }
                    }
                    let transaction = await models.sequelize.transaction();
                    try {
                        await materia.create(data);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado los datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}
module.exports = MateriaController;