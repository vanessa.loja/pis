'use strict';
var models = require('../models');
var cuenta = models.cuenta;
const { body, validationResult, check } = require('express-validator');
const bcypt = require('bcrypt');
const salRounds = 8;
let jwt = require('jsonwebtoken');

class CuentaController{
async sesion(req, res) {
  let errors = validationResult(req);
  if (errors.isEmpty()) {
    var login = await cuenta.findOne({
      where: { email: req.body.email },
      include: [
        {
          model: models.estudiante,
          as: 'estudiante',
          attributes: ['nombres', 'apellidos']
        },
        {
            model:models.profesor,
            as:'profesor',
            attributes:['nombres','apellidos']
        }
      ]
    }

    );
    if (!login) {
      res.status(400);
      res.json({
        msg: 'CUENTA NO ENCONTRADA',
        code: 400
      });
      return;
    }

    if(login.estudiante === null ){
        if (login.profesor != "") {
            res.status(200);

            var isClaveValida = function (clave, claveUs) {
              return bcypt.compareSync(claveUs, clave);
            };
            if (login.estado) {
                if (isClaveValida(login.clave, req.body.clave)) {
                  let tipoUsuario;
                  if (login.profesor) {
                    tipoUsuario = 'profesor';
                  } else {
                    tipoUsuario = 'estudiante';
                  }
        
                  const tokenData = {
                    external: login.external_id,
                    email: login.email,
                    check: true,
                    userType: tipoUsuario
                  };
        
                  require('dotenv').config();
                  const llave = process.env.KEY;
                  const token = jwt.sign(tokenData, llave, { expiresIn: "1h" });
                  res.json({
                    msg: 'OK!',
                    token: token,
                    user: login.profesor.nombres + ' ' + login.profesor.apellidos,
                    userType: tipoUsuario,
                    code: 200,
                    email: login.email
                  });
                } else {
                  res.json({
                    msg: "CLAVE INCORRECTA",
                    code: 400
                  });
                  res.status(400);
                }
              } else {
                res.json({
                  msg: "CUENTA DESACTIVADA",
                  code: 400
                });
              }
        } else if(login.estudiante === null) {
            res.status(400);
        res.json({
          msg: "CUENTA NO ENCONTRADA",
          code: 400
        });
        }
    }else if(login.estudiante!= ""){
        res.status(200);

        var isClaveValida = function (clave, claveUs) {
          return bcypt.compareSync(claveUs, clave);
        };
        if (login.estado) {
            if (isClaveValida(login.clave, req.body.clave)) {
              let tipoUsuario;
              if (login.estudiante) {
                tipoUsuario = 'estudiante';
              } else {
                tipoUsuario = 'profesor';
              }
    
              const tokenData = {
                external: login.external_id,
                email: login.email,
                check: true,
                userType: tipoUsuario
              };
    
              require('dotenv').config();
              const llave = process.env.KEY;
              const token = jwt.sign(tokenData, llave, { expiresIn: "1h" });
              res.json({
                msg: 'OK!',
                token: token,
                user: login.estudiante.nombres + ' ' + login.estudiante.apellidos,
                userType: tipoUsuario,
                code: 200,
                email: login.email
              });
            } else {
              res.json({
                msg: "CLAVE INCORRECTA",
                code: 400
              });
              res.status(400);
            }
          } else {
            res.json({
              msg: "CUENTA DESACTIVADA",
              code: 400
            });
          }
    } else if(login.estudiante === null) {
        res.status(400);
        res.json({
          msg: "CUENTA NO ENCONTRADA",
          code: 400
        });
    }
  } else {
    res.status(400);
    res.json({ msg: "Faltan datos", code: 400 });
    
   }
}


} 
module.exports =CuentaController;
