'use strict';
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
  const cursa = sequelize.define('cursa', {
    id_materia: {type: DataTypes.INTEGER,allowNull: false},
    id_profesor: { type: DataTypes.INTEGER,allowNull: false},
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    id_estudiante: {type: DataTypes.INTEGER,allowNull: false},
    nota: {type: DataTypes.DOUBLE,allowNull:true}
  }, { freezeTableName: true });

  cursa.associate = function (models) {
    cursa.belongsTo(models.materia, { foreignKey: 'id_materia', as: 'materia' });
    cursa.belongsTo(models.profesor, { foreignKey: 'id_profesor', as: 'profesor' });
    cursa.belongsTo(models.estudiante, { foreignKey: 'id_estudiante', as: 'estudiante' });
  };

  return cursa;
};
