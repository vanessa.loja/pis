
import React from 'react';
import ReactDOM from 'react-dom';
import '../css/login.css';
import { useForm } from 'react-hook-form';
import { mensajes } from '../utilidades/Mensaje';
import { InicioSesion } from '../hooks/Conexion';
import { saveToken } from '../utilidades/Sessionutil';
import { useNavigate, useParams } from 'react-router-dom';
import facebookIcon from '../img/facebook.png';
import googleIcon from '../img/google.png';


const Login=()=> {
  const { register, handleSubmit } = useForm();
  const navegation = useNavigate();
  const onSubmit = (data) => {
    if (!data.email || !data.clave) {
      mensajes('Faltan datos', 'error', 'Campos vacíos');
      return;
    }

    var datos = {
      email: data.email,
      clave: data.clave
    };

    InicioSesion(datos)
      .then((info) => {
        if (info.msg === 'CLAVE INCORRECTA') {
          mensajes(info.message, 'error', 'Clave Incorrecta');
        } else if (info.msg === 'CUENTA NO ENCONTRADA') {
          mensajes(info.message, 'error', 'Cuenta no encontrada');
        } else {
          console.log(info);
          if (info.userType === "profesor") {

            mensajes(info.message, 'success', 'Exitoso');
            saveToken(info.token);
            navegation('/IndiceProfesor')
          } else if (info.userType === "estudiante") {
            mensajes(info.message, 'success', 'Exitoso');
            saveToken(info.token);
            navegation('/Indice');
          }

         
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  
  return (
    <div>
      {/* particles.js container */}
      <div id="particles-js"></div>
      {/* Login */}
      <div className="loginBox">
        <div className="fromBox">
          <div className="buttonBox">
            <div id="elegir"></div>
            <button type="button" className="presionarBtn" onClick={login}>
              Iniciar Sesión
            </button>
            
          </div>
          <div className="redes">
            
            <img src={facebookIcon} alt="iconofacebook" />
            <img src ={googleIcon}alt="icono_google" />
          </div>
          
          <form id="login" className="inputGroup">
            <div className="userBox">
            <input type="email" id="typeEmailX"{...register('email')} required />
        <label>Correo</label>
            </div>
            <div className="userBox">
            <input type="password"{...register('clave')} required />
                     <label>Contraseña</label>
            </div>
            <a onClick={handleSubmit(onSubmit)}>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              Acceder
            </a>
          </form>
          <form id="registrar" className="inputGroup">
            <div className="userBox">
              <input type="text" name="" required="" />
              <label>Nombre de usuario</label>
            </div>
            <div className="userBox">
              <input type="email" name="" required="" />
              <label>Correo</label>
            </div>
            <div className="userBox">
              <input type="password" name="" required="" />
              <label>Contraseña</label>
            </div>
            <a href="#">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              Registrar
            </a>
          </form>
        </div>
      </div>
    </div>
  );
}

function login() {
  const x = document.getElementById("login");
  const y = document.getElementById("registrar");
  const z = document.getElementById("elegir");
  x.style.left = "50px";
  y.style.left = "450px";
  z.style.left = "0px";
}

function registrar() {
  const x = document.getElementById("login");
  const y = document.getElementById("registrar");
  const z = document.getElementById("elegir");
  x.style.left = "-400px";
  y.style.left = "50px";
  z.style.left = "120px";
}

export default Login;