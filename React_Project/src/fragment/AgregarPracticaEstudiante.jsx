import React, { useState } from 'react';

const AgregarPracticaEstudiante = () => {
    return (
        <section className="vh-100" >
             
            <div className="container h-100">
            <center><h1>Subir Tarea</h1></center>
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-xl-9">
                        <div className="card" >
                            <div className="card-body">

                            <div className="row align-items-center py-3">
                               
                                                <div className="col-md-3 ps-5">

                                                    <h6 className="mb-0">ADJUNTO: </h6>

                                                </div>
                                                <div className="col-md-9 pe-5">

                                                    <input className="form-control form-control-lg" id="formFileLg" type="file" />
                                                    <div className="small text-muted mt-2">Upload your archive/. Max file size 50 MB</div>

                                                </div>
                                            </div>
                                            <hr className="mx-n3"/>
                                <div className="row align-items-center pt-4 pb-3">
                                    <div className="col-md-3 ps-5">

                                        <h6 className="mb-0">GUARDAR COMO: </h6>

                                    </div>
                                    <div className="col-md-9 pe-5">

                                        <input type="text" className="form-control form-control-lg" />

                                    </div>
                                </div>

                                <hr className="mx-n3"/>

                                    <div className="row align-items-center py-3">
                                        <div className="col-md-3 ps-5">

                                            <h6 className="mb-0">AUTOR: </h6>

                                        </div>
                                        <div className="col-md-9 pe-5">

                                            <input type="email" className="form-control form-control-lg" placeholder="" />

                                        </div>
                                    </div>
                                    <hr className="mx-n3"/>
                                    <div className="col-md-13 pe-1">

                                        <textarea className="form-control" rows="2" placeholder="Send a Message"></textarea>

                                    </div>

                                    <hr className="mx-n3"/>

                                    <div className="px-5 py-4">
                                        <button type="submit" className="btn btn-primary btn-lg">SUBIR ESTE ARCHIVO</button>
                                 
                                    </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
        </section>
                    
    );
}
export default AgregarPracticaEstudiante ;